CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Useful Drush Commands module provides various Drush commands which can be
used by developers and site builders to fasten up the development process.

 * For a full description of the module visit:
   https://www.drupal.org/project/drush_useful

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/drush_useful


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Useful Drush Commands module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

Navigate to Administration > Extend and enable the module.
No configuration is necessary.

This module provides various Drush commands which can be used by developers and
site builders to fasten up the development process.

Currently, it provides following Drush commands:

 * useful-developer-install(idev): Install all the important development modules
   such as Devel, Admin Toolbar, etc.

 * useful-developer-uninstall(udev): Uninstall development modules such as
   Devel, Admin Toolbar, etc. and make website production ready.

 * useful-seo-install(iseo): Install all the modules related to SEO of the
   website such as Metatag, Simple Sitemap, Yoast SEO, and Pathauto.

 * useful-seo-uninstall(useo): Uninstall all the SEO related modules.

 * useful-optimize-install(iopt): Install optimization related modules such as
   Redis, Advance aggregation, Fast 404, etc.

 * useful-optimize-uninstall(uopt): Uninstall optimization modules such as
   Redis, Advance aggregation, Fast 404, etc. Could be used in debugging and
   testing phase.

 * useful-migration-install(imig): Install Migration Modules such as Migrate,
   Migrate Plus, Migrate Tools, etc.

 * useful-migration-uninstall(umig): Uninstall Migration Modules such as
   Migrate, Migrate Plus, Migrate Tools, etc. Could be useful when you have
   completed the migration and don't require these modules anymore.

 * useful-decoupled-install(idcoup): Install Modules such as RestAPI, REST UI,
   JSON API, and Basic Auth. Now you go headless in single Drush command.

 * useful-decoupled-uninstall(udcoup): Uninstall Modules such as RestAPI, REST
   UI, JSON API, and Basic Auth.

 * useful-security-install(isec): Install Security related modules such as
   Captcha, Paranoia, etc.

 * useful-security-uninstall(usec): Uninstall Security related modules such as
   Captcha, Paranoia, etc.

 * useful-search-install(isrch): Install Search enhancing modules such as Search
   API, Search API DB, Facets, etc.

 * useful-search-uninstall(usrch): Uninstall Search enhancing modules such as
   Search API, Search API DB, Facets, etc.

 * useful-social-install(isoc): Install social sharing modules like AddToAny,
   Share This, etc.

 * useful-social-install(usoc): Uninstall social sharing modules like AddToAny,
   Share This, etc.

This project is still in the development version and more useful commands will
be added soon. The main aim of the module is to save developers time while
performing common operations in Drupal.


MAINTAINERS
-----------

 * Gaurav Kapoor (gaurav.kapoor) - https://www.drupal.org/u/gauravkapoor

Supporting organization:

 * OpenSense Labs - https://www.drupal.org/opensense-labs
