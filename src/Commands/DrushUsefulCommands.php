<?php

namespace Drupal\drush_useful\Commands;

use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class DrushUsefulCommands extends DrushCommands {

  /**
   * Install all important development modules like Devel, Admin Toolbar etc.
   *
   * @command useful:developer-install
   * @aliases idev,useful-developer-install
   */
  public function developerInstall() {
    $modules = ['devel', 'admin_toolbar'];
    foreach ($modules as $value) {
      $command = "composer require drupal/" . $value;
      system($command);
    }
    drush_invoke_process('@self', 'pm-enable', $modules);
    drush_invoke_process('@self', 'pm-enable', [
      'webprofiler',
      'devel_generate',
      'kint',
      'admin_toolbar_tools',
    ]);
  }

  /**
   * Uninstall development modules such as Devel, Admin Toolbar etc.
   *
   * @command useful:developer-uninstall
   * @aliases udev,useful-developer-uninstall
   */
  public function developerUninstall() {
    drush_invoke_process('@self', 'pm-uninstall', [
      'webprofiler',
      'devel_generate',
      'kint',
      'admin_toolbar_tools',
    ]);
    drush_invoke_process('@self', 'pm-uninstall', ['devel', 'admin_toolbar']);
  }

  /**
   * Install all the modules related to SEO of the website.
   *
   * @command useful:seo-install
   * @aliases iseo,useful-seo-install
   */
  public function seoInstall() {
    $modules = [
      'metatag',
      'pathauto',
      'simple_sitemap',
      'google_analytics',
      'redirect',
      'schema_metatag',
      'yoast_seo',
    ];
    foreach ($modules as $value) {
      $command = "composer require drupal/" . $value;
      system($command);
    }
    drush_invoke_process('@self', 'pm-enable', $modules);
    drush_invoke_process('@self', 'pm-enable', ['metatag_views']);
  }

  /**
   * Uninstall all the modules related to SEO of the website.
   *
   * @command useful:seo-uninstall
   * @aliases useo,useful-seo-uninstall
   */
  public function seoUninstall() {
    drush_invoke_process('@self', 'pm-uninstall', ['metatag_views']);
    drush_invoke_process('@self', 'yoastseo-prepare-uninstall');
    drush_invoke_process('@self', 'pm-uninstall', [
      'pathauto',
      'simple_sitemap',
      'google_analytics',
      'redirect',
      'schema_metatag',
      'yoast_seo',
    ]);
  }

  /**
   * Install optimization modules such as Redis, Advagg, Fast 404 etc.
   *
   * @command useful:optimize-install
   * @aliases iopt,useful-optimize-install
   */
  public function optimizeInstall() {
    $modules = ['advagg', 'redis', 'fast_404'];
    foreach ($modules as $value) {
      $command = "composer require drupal/" . $value;
      system($command);
    }
    drush_invoke_process('@self', 'pm-enable', $modules);
    drush_invoke_process('@self', 'pm-enable', [
      'advagg_old_ie_compatibility',
      'advagg_css_minify',
      'advagg_cdn',
      'advagg_bundler',
      'advagg_mod',
      'advagg_ext_minify',
      'advagg_validator',
      'advagg_js_minify',
    ]);
  }

  /**
   * Uninstall optimization modules such as Redis, Advagg, Fast 404 etc.
   *
   * @command useful:optimize-uninstall
   * @aliases uopt,useful-optimize-uninstall
   */
  public function optimizeUninstall() {
    drush_invoke_process('@self', 'pm-uninstall', [
      'advagg_old_ie_compatibility',
      'advagg_css_minify',
      'advagg_cdn',
      'advagg_bundler',
      'advagg_mod',
      'advagg_ext_minify',
      'advagg_validator',
      'advagg_js_minify',
    ]);
    drush_invoke_process('@self', 'pm-uninstall', [
      'advagg',
      'redis',
      'fast_404',
    ]);
  }

  /**
   * Install Migration Modules such as migrate, migrate_plus, migrate_tools etc.
   *
   * @command useful:migration-install
   * @aliases imig,useful-migration-install
   */
  public function migrationInstall() {
    $modules = ['migrate_plus', 'migrate_tools'];
    foreach ($modules as $value) {
      $command = "composer require drupal/" . $value;
      system($command);
    }
    drush_invoke_process('@self', 'pm-enable', $modules);
    drush_invoke_process('@self', 'pm-enable', [
      'migrate',
      'migrate_drupal',
      'migrate_drupal_ui',
      'migrate_example_advanced_setup',
      'migrate_example_advanced',
      'migrate_example_setup',
      'migrate_example',
    ]);
  }

  /**
   * Uninstall Migration Modules such as migrate, migrate_plus, etc.
   *
   * @command useful:migration-uninstall
   * @aliases umig,useful-migration-uninstall
   */
  public function migrationUninstall() {
    drush_invoke_process('@self', 'pm-uninstall', ['migrate_plus', 'migrate_tools']);
    drush_invoke_process('@self', 'pm-uninstall', [
      'migrate',
      'migrate_drupal',
      'migrate_drupal_ui',
      'migrate_example_advanced_setup',
      'migrate_example_advanced',
      'migrate_example_setup',
      'migrate_example',
    ]);
  }

  /**
   * Install Migration Modules such as   RestAPI, REST UI, jsonAPI ,basic_Auth,.
   *
   * @command useful:decoupled-install
   * @aliases idcoup,useful-decoupled-install
   */
  public function decoupledInstall() {
    $modules = ['restui', 'jsonapi', 'basic_auth', 'rest'];
    foreach ($modules as $value) {
      $command = "composer require drupal/" . $value;
      system($command);
    }
    drush_invoke_process('@self', 'pm-enable', $modules);
  }

  /**
   * Uninstall Migration Modules such as RestAPI, REST UI, jsonAPI, basic_Auth.
   *
   * @command useful:decoupled-uninstall
   * @aliases udcoup,useful-decoupled-uninstall
   */
  public function decoupledUninstall() {
    drush_invoke_process('@self', 'pm-uninstall', [
      'restui',
      'jsonapi',
      'basic_auth',
      'rest',
    ]);
  }

  /**
   * Install Security related modules such as captcha,paranoia etc.
   *
   * @command useful:security-install
   * @aliases isec,useful-security-install
   */
  public function securityInstall() {
    $modules = ['captcha', 'recaptcha', 'paranoia', 'tfa', 'password_strength'];
    foreach ($modules as $value) {
      $command = "composer require drupal/" . $value;
      system($command);
    }
    drush_invoke_process('@self', 'pm-enable', $modules);
    drush_invoke_process('@self', 'pm-enable', ['image_captcha']);
  }

  /**
   * Uninstall Security related modules such as captcha,paranoia etc.
   *
   * @command useful:security-uninstall
   * @aliases usec,useful-security-uninstall
   */
  public function securityUninstall() {
    drush_invoke_process('@self', 'pm-uninstall', ['image_captcha']);
    drush_invoke_process('@self', 'pm-uninstall', [
      'captcha',
      'recaptcha',
      'paranoia',
      'tfa',
      'password_strength',
    ]);
  }

  /**
   * Install Search enhancing modules such as Search API, Search API DB, etc.
   *
   * @command useful:search-install
   * @aliases isrch,useful-search-install
   */
  public function searchInstall() {
    $modules = ['search_api', 'facets'];
    foreach ($modules as $value) {
      $command = "composer require drupal/" . $value;
      system($command);
    }
    drush_invoke_process('@self', 'pm-enable', $modules);
    drush_invoke_process('@self', 'pm-enable', [
      'search_api_glossary',
      'search_api_sorts',
    ]);
    drush_invoke_process('@self', 'pm-enable', [
      'search_api_views_taxonomy',
      'search_api_db_defaults',
      'search_api_db',
    ]);
  }

  /**
   * Uninstall search modules such as Search API, Search API DB, Facets etc.
   *
   * @command useful:search-uninstall
   * @aliases usrch,useful-search-uninstall
   */
  public function searchUninstall() {
    drush_invoke_process('@self', 'pm-uninstall', [
      'search_api_views_taxonomy',
      'search_api_db_defaults',
      'search_api_db',
    ]);
    drush_invoke_process('@self', 'pm-uninstall', [
      'search_api_glossary',
      'search_api_sorts',
    ]);
    drush_invoke_process('@self', 'pm-uninstall', ['search_api', 'facets']);
  }

  /**
   * Install social sharing modules like  AddToAny , sharethis etc.
   *
   * @command useful:social-install
   * @aliases isoc,useful-social-install
   */
  public function socialInstall() {
    $modules = ['addtoany', 'sharethis'];
    foreach ($modules as $value) {
      $command = "composer require drupal/" . $value;
      system($command);
    }
    drush_invoke_process('@self', 'pm-enable', $modules);
  }

  /**
   * Uninstall social sharing modules like  AddToAny , sharethis etc.
   *
   * @command useful:social-uninstall
   * @aliases usoc,useful-social-uninstall
   */
  public function socialUninstall() {
    drush_invoke_process('@self', 'pm-uninstall', ['sharethis', 'addtoany']);
  }

  /**
   * Install  modules  like faq,tour_ui.
   *
   * @command useful:education-install
   * @aliases iedu,useful-education-install
   */
  public function educationInstall() {
    $modules = ['faq', 'tour_ui'];
    foreach ($modules as $value) {
      $command = "composer require drupal/" . $value;
      system($command);
    }
    drush_invoke_process('@self', 'pm-enable', $modules);
  }

  /**
   * Uninstall  modules like faq,tour_ui.
   *
   * @command useful:education-uninstall
   * @aliases uedu,useful-education-uninstall
   */
  public function educationUninstall() {
    drush_invoke_process('@self', 'pm-uninstall', ['faq', 'tour_ui']);
  }

  /**
   * Install  modules  like  google_adsense, dfp, mailchip, simpleads etc.
   *
   * @command useful:advertise-install
   * @aliases iadv,useful-advertise-install
   */
  public function advertiseInstall() {
    $modules = ['mailchimp', 'adsense', 'dfp', 'simpleads'];
    foreach ($modules as $value) {
      $command = "composer require drupal/" . $value;
      system($command);
    }
    drush_invoke_process('@self', 'pm-enable', $modules);
  }

  /**
   * Uninstall  modules  like  google_adsense, dfp, mailchip, simpleads etc.
   *
   * @command useful:advertise-uninstall
   * @aliases uadv,useful-advertise-uninstall
   */
  public function advertiseUninstall() {
    drush_invoke_process('@self', 'pm-uninstall', [
      'mailchimp',
      'adsense',
      'dfp',
      'simpleads',
    ]);
  }

  /**
   * Install  modules  like views_slideshow, views_accordion.
   *
   * @command useful:contentdisplay-install
   * @aliases icd,useful-contentdisplay-install
   */
  public function contentdisplayInstall() {
    $modules = ['views_slideshow', 'views_accordion'];
    foreach ($modules as $value) {
      $command = "composer require drupal/" . $value;
      system($command);
    }
    drush_invoke_process('@self', 'pm-enable', $modules);
  }

  /**
   * Uninstall  modules  like views_slideshow, views_accordion.
   *
   * @command useful:contentdisplay-uninstall
   * @aliases ucd,useful-contentdisplay-uninstall
   */
  public function contentdisplayUninstall() {
    drush_invoke_process('@self', 'pm-uninstall', [
      'views_slideshow',
      'views_accordion',
    ]);
  }

  /**
   * Install modules like views_data_export, contentimport, backup_db etc.
   *
   * @command useful:importexport-install
   * @aliases iimex,useful-importexport-install
   */
  public function importexportInstall() {
    $modules = ['contentimport ', 'printable'];
    foreach ($modules as $value) {
      $command = "composer require drupal/" . $value;
      system($command);
    }
    drush_invoke_process('@self', 'pm-enable', [
      'contentimport',
      'printable',
    ]);
  }

  /**
   * Uninstall modules like views_data_export, contentimport ,backup_db, etc.
   *
   * @command useful:importexport-uninstall
   * @aliases uimex,useful-importexport-uninstall
   */
  public function importexportUninstall() {
    drush_invoke_process('@self', 'pm-uninstall', [
      'contentimport',
      'printable',
    ]);
  }

}
